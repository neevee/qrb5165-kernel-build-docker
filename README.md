# qrb5165-kernel-build

QRB5165 Kernel Build Docker Image Project.

## Summary

This project is used to generate a docker image that can then be used as a build host.  This means you don't have to deal with setting up tools, which can be a great deal of pain!

It does mean you need to have a bare minimum knowledge of git, bash and Docker.

## Requirements

- Git
- Docker
- The more processors, the better!

## To Build Image

```
git clone https://gitlab.modalai.com/modalai/voxl2-private/qrb5165-kernel-build-docker
cd qrb5165-kernel-build-docker

./docker-build-image.sh
```

## To Use

See [User Guide](https://docs.modalai.com/voxl2-kernel-build-guide)

## Details

### docker-build-image.sh

Script that will generate a docker image, with name and version specified in `common`.

### docker-run-image.sh

Script that will start a container based on this docker image, mounting the current working directory into the workspace, giving you an interactive bash shell.

### common

A file called `common` lives in the root of the project:

```
# docker container name/ver
BUILD_OUTPUT=workspace
IMAGE_NAME=qrb5165-kernel-build
IMAGE_TAG=14.1
```

A few scripts source these values, so they're shared via sourcing this `common` file in the start of scripts.