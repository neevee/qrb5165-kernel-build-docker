#!/bin/bash
source common

if ! [ -d "${BUILD_OUTPUT}" ]; then
    mkdir "${BUILD_OUTPUT}" || exit 1
fi

docker run --rm -it --privileged \
    --mount type=bind,source="$(pwd)"/"${BUILD_OUTPUT}",target=/home/user/build_mount \
    "${IMAGE_NAME}":"${IMAGE_TAG}" /bin/bash
