#!/bin/bash


## -----------------------------------------------------------------------------------------------------------------------------
## Global Variables
## -----------------------------------------------------------------------------------------------------------------------------
BUILD_ID="lu.um.1.2.1"

# Paths
WORKSPACE="$(pwd)"
PATCH_ROOT="${WORKSPACE}"/patches
APPS_ROOT="${WORKSPACE}"/build_mount/"${BUILD_ID}"/apps_proc


## -----------------------------------------------------------------------------------------------------------------------------
## Validate environment is setup correctly
## -----------------------------------------------------------------------------------------------------------------------------
_validate_setup() {
	if [ ! -d "${APPS_ROOT}" ]; then
	    echo "[ERROR] Missing apps_proc root"
		exit 1
	elif [ ! -d "${PATCH_ROOT}" ]; then
		echo "[ERROR] Missing patches root"
		exit 1
	fi

	echo "[INFO] All paths found"
}


## -----------------------------------------------------------------------------------------------------------------------------
## Apply patches to build
## -----------------------------------------------------------------------------------------------------------------------------
_apply_kernel_patches() {

	_validate_setup

	# Remove ROS 
	cd "${APPS_ROOT}"/poky/meta-qti-ubuntu
	patch -p1 -uN <	"${PATCH_ROOT}"/14.1a-META-QTI-UBUNTU-REMOVE-ROS2.patch
	cd "${APPS_ROOT}"/poky/meta-qti-bsp
	patch -p1 -uN <	"${PATCH_ROOT}"/14.1a-META-QTI-BSP-REMOVE-ROS2.patch
	cd "${APPS_ROOT}"/poky/meta-qti-robotics
	patch -p1 -uN <	"${PATCH_ROOT}"/14.1a-META-QTI-ROBOTICS-REMOVE-ROS2.patch

	# Remove public key to allow for unsigned kernel and add custom kernel path
	cd "${APPS_ROOT}"/poky/meta-qti-bsp
	patch -p1 -uN <	"${PATCH_ROOT}"/14.1a-META-QTI-BSP-REMOVE-PUBLIC-KEY.patch  
	patch -p1 -uN < "${PATCH_ROOT}"/14.1a-META-QTI-BSP-ADD-KERNEL-PATH.patch    

	# Update recipes to include newer version of packages
	cd "${APPS_ROOT}"/poky/meta-qti-ubuntu
	patch -p1 -uN <	"${PATCH_ROOT}"/14.1a-META-QTI-UBUNTU-UPDATE-LIBEXPAT1.patch
	patch -p1 -uN <	"${PATCH_ROOT}"/14.1a-META-QTI-UBUNTU-UPDATE-LIBXML.patch
	patch -p1 -uN < "${PATCH_ROOT}"/14.1a-META-QTI-UBUNTU-UPDATE-GST-PLUGINS-GOOD.patch
	patch -p1 -uN < "${PATCH_ROOT}"/14.1a-META-QTI-UBUNTU-DEBDL-PREMIRROR.patch
	patch -p1 -uN < "${PATCH_ROOT}"/14.1a-META-QTI-UBUNTU-RM-SRC_URI-DLMGR.patch

	# Remove kernel config path so our machine can set it
	cd "${APPS_ROOT}"/poky/meta-qti-ubuntu
	patch -p1 -uN < "${PATCH_ROOT}"/14.1a-META-QTI-UBUNTU-REMOVE-KERNEL-CONFIG-PATH.patch

	# Make kernel headers not include perf extension
	cd "${APPS_ROOT}"/poky/meta-qti-ubuntu
	patch -p1 -uN < "${PATCH_ROOT}"/14.1a-META-QTI-UBUNTU-REMOVE-PERF.patch

	cd "${APPS_ROOT}"/poky/meta-voxl2-bsp
	patch -p1 -uN < "${PATCH_ROOT}"/14.1a-META-VOXL2-BSP-ADD-QCLASSES.patch

	# Remove packages from core image install since we are only building kernel
	# and prop deps aren't available
	cd "${APPS_ROOT}"/poky/meta-qti-ubuntu
	patch -p1 -uN < "${PATCH_ROOT}"/14.1a-META-QTI-UBUNTU-MODIFY-CORE-PKGS.patch
	patch -p1 -uN < "${PATCH_ROOT}"/14.1a-META-QTI-UBUNTU-RM-AUDIO-CORE-PKGS.patch

	echo "[INFO] - Done patching kernel"
}

_apply_kernel_patches $@


