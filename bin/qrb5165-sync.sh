#!/bin/bash


## -----------------------------------------------------------------------------------------------------------------------------
## Global Variables
## -----------------------------------------------------------------------------------------------------------------------------
BUILD_ID="lu.um.1.2.1"
REPO_MANIFEST_FILE="LU.UM.1.2.1.r1-30500-QRB5165.0.xml"
REPO_MANIFEST_URL="https://git.codelinaro.org/clo/le/le/manifest"

# Paths
WORKSPACE="$(pwd)"
BUILD_MOUNT_POINT=${WORKSPACE}/build_mount
APPS_ROOT=${BUILD_MOUNT_POINT}/${BUILD_ID}/apps_proc
POKY_ROOT=${BUILD_MOUNT_POINT}/${BUILD_ID}/apps_proc/poky
KERNEL_DIR=${APPS_ROOT}/src/kernel/

# Predownloaded packages
DEBDL_PACKAGE_NAME=debdl_premirror_14.1a_0.1.tar.gz


## -----------------------------------------------------------------------------------------------------------------------------
## Unzip qcom chipcode for build
## -----------------------------------------------------------------------------------------------------------------------------
_unzip_chipcode() {
    
    # Fix permissions
    echo "user" | sudo -S chown user:user "${BUILD_MOUNT_POINT}"

    mkdir -p "${APPS_ROOT}"
    cd "${APPS_ROOT}" || exit 1

    # prevent git from prompting us such that this requires no user intervention
    git config --global color.ui false

    git config --global user.email "nvhariharan@neeveetech.com"
    git config --global user.name "Mr.Hariharan"

    echo "[INFO] - Syncing CLO repo..."
    repo init -u "${REPO_MANIFEST_URL}" -b release -m "${REPO_MANIFEST_FILE}"
    repo sync -c --no-tags -j"$(nproc)"
}


## -----------------------------------------------------------------------------------------------------------------------------
## Modify build workspace to include custom tweaks
## -----------------------------------------------------------------------------------------------------------------------------
_setup_workspace() {
    cd "${POKY_ROOT}" || exit 1

    # Discard unused layers
    rm -rf meta-qti-qsap
    rm -rf meta-qti-wlan

    echo "[INFO] - Syncing debian dl premirror"
    wget https://storage.googleapis.com/voxl2-mirrors/$DEBDL_PACKAGE_NAME
    tar -xvf $DEBDL_PACKAGE_NAME -C $APPS_ROOT

    echo "[INFO] - Syncing meta-voxl2 repo"
    git clone -b dev https://gitlab.com/voxl-public/system-image-build/meta-voxl2.git

    echo "[INFO] - Syncing meta-voxl2-bsp repo"
    git clone -b dev https://gitlab.com/neevee/meta-voxl2-bsp.git

    echo "[INFO] - Syncing Kernel"
    cd "${KERNEL_DIR}" || exit 1
    git clone -b dev https://gitlab.com/neevee/qrb5165-kernel.git
    mv qrb5165-kernel msm-qrb5165-4.19
}


## -----------------------------------------------------------------------------------------------------------------------------
## Begin kernel sync
## -----------------------------------------------------------------------------------------------------------------------------
_sync_kernel() {
    _unzip_chipcode
    _setup_workspace

    echo "[INFO] - Done syncing kernel"
}

_sync_kernel $@








