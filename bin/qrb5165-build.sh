#!/bin/bash


## -----------------------------------------------------------------------------------------------------------------------------
## Global Variables
## ----------------------------------------------------------------------------------------------------------------------------- 
PLATFORM_TYPE=""
BUILD_MODE="debug"
BUILD_ID="lu.um.1.2.1"


## -----------------------------------------------------------------------------------------------------------------------------
## Help message print
## ----------------------------------------------------------------------------------------------------------------------------- 
_print_usage() {
	echo ""
	echo "Build QRB5165 kernel"
	echo "Usage ./qrb5165-build.sh --platform <PLATFORM> [--perf]"
	echo ""
	echo "Arguments"
	echo "	-h, --help      Show this help message and exit"
	echo ""
	echo "	-m, --machine   Select build machine (M0052 | M0054)"
	echo "                  This argument is required"
	echo ""
	echo "	-p, --perf      Enable perf build, by default debug kernel"
	echo "                  will build"
	echo ""
}


## -----------------------------------------------------------------------------------------------------------------------------
## Make sure all required areguments are set
## ----------------------------------------------------------------------------------------------------------------------------- 
_validate_args() {
    # Check for correct platform
	if [ "$PLATFORM_TYPE" != "m0052" ] && [ "$PLATFORM_TYPE" != "m0054" ]; then
        if [ "$PLATFORM_TYPE" == "" ]; then
            echo "[ERROR] Selecting platform is required"
        else
            echo "[ERROR] Platform type is unsupported: $PLATFORM_TYPE"
        fi

		_print_usage
		exit 1
	fi
}


## -----------------------------------------------------------------------------------------------------------------------------
## Print meta for simple user validation
## ----------------------------------------------------------------------------------------------------------------------------- 
_print_meta_info() {
    echo "[INFO] Starting Build"
    echo "[INFO]    Platform:   $PLATFORM_TYPE"
    echo "[INFO]    Build ID:   $BUILD_ID"
    echo "[INFO]    Build Type: $BUILD_MODE"

    sleep 2
}


## -----------------------------------------------------------------------------------------------------------------------------
## Parse arguments                            
## ----------------------------------------------------------------------------------------------------------------------------- 
_parse_opts() {
    while [[ $# -gt 0 ]]; do
        
		# convert argument to lower case for robustness
        arg=$(echo "$1" | tr '[:upper:]' '[:lower:]')

		# parse arguments
        case ${arg} in
            "h"|"-h"|"help"|"--help")
                _print_usage
                exit 0
                ;;
            "m"|"-m"|"machine"|"--machine")
				PLATFORM_TYPE=$(echo "$2" | tr '[:upper:]' '[:lower:]')
				shift
                ;;
            "p"|"-p"|"perf"|"--perf")
				BUILD_MODE="perf"
                ;;
            *)
                echo "Unknown argument $1"
                _print_usage
                exit -1
                ;;
        esac
        shift
    done

	# Make sure our args have been correctly populated, and print
	_validate_args
}


## -----------------------------------------------------------------------------------------------------------------------------
## Main kernel build logic
## ----------------------------------------------------------------------------------------------------------------------------- 
_build_kernel() {
	# Declare paths
	BUILD_ID="lu.um.1.2.1"
	WORKSPACE="$(pwd)"
	BUILD_MOUNT_POINT=${WORKSPACE}/build_mount
	APPS_ROOT=${BUILD_MOUNT_POINT}/${BUILD_ID}/apps_proc

	cd "${APPS_ROOT}" || exit 1

	# Begin build
	MACHINE=${PLATFORM_TYPE} DISTRO=qti-distro-ubuntu-fullstack-debug source poky/qti-conf/set_bb_env.sh

	export PREBUILT_SRC_DIR="${APPS_ROOT}/prebuilt_HY11"

	echo "[INFO] - building kernel"
	bitbake linux-msm -c cleanall
	bitbake linux-msm
	bitbake qti-ubuntu-robotics-image -fc do_make_bootimg
	echo "[INFO] - Done building kernel"
}

_parse_opts $@
_print_meta_info
_build_kernel
