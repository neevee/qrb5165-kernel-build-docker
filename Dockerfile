################################################################################
# Copyright 2021 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

FROM ubuntu:18.04

RUN apt-get -y update
RUN apt-get update --fix-missing

# From QC Docs
RUN apt-get -y install texinfo chrpath gcc-aarch64-linux-gnu libarchive-dev ssh libselinux1-dev fakechroot g++-aarch64-linux-gnu libiberty-dev qemu-user-static g++ gawk gcc make fakeroot diffstat libxml-simple-perl docbook2x zlib1g-dev

# to fix build errors
RUN apt-get -y install sudo diffstat cpio libxml-simple-perl locales python-setuptools python3-pip

# for this docker, utils to help debug setup
RUN apt-get -y install unzip vim rsync git curl

# See 80-PV086-300 D, September 16, 2020
RUN rm -rf /lib/ld-linux-aarch64.so.1
RUN ln -sf /usr/aarch64-linux-gnu/lib/ld-2.27.so /lib/ld-linux-aarch64.so.1
RUN ln -sf /bin/bash /bin/sh

# Set the locale, needed for python and to fix error during build
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && locale-gen
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

# bitbake needs to run under on non-root user.  Add a non-root user with the following details:
# username: user, password: user, Home: /home/user, Groups: sudo, user
RUN /usr/sbin/useradd -m -s /bin/bash -G sudo user && echo user:user | /usr/sbin/chpasswd

WORKDIR /home/user/


# Copy all patches into build
COPY --chown=user:user patches/ ./patches

# needed for build
RUN wget https://dl.google.com/go/go1.14.4.linux-amd64.tar.gz
RUN tar -C /usr/local -xzf go1.14.4.linux-amd64.tar.gz

# copy our build scripts into docker contianer
COPY --chown=user:user bin/qrb5165-sync.sh .
COPY --chown=user:user bin/qrb5165-build.sh .
COPY --chown=user:user bin/qrb5165-patch.sh .
RUN chmod +x qrb5165-sync.sh
RUN chmod +x qrb5165-patch.sh
RUN chmod +x qrb5165-build.sh

# Install repo tool and move to a .bin
RUN curl https://storage.googleapis.com/git-repo-downloads/repo > /usr/bin/repo
RUN chmod a+rx /usr/bin/repo

USER user

RUN export PATH=$PATH:/usr/local/go/bin
